import axios from "axios";
import { useEffect, useState } from "react";
import { environment } from "src/environment/environment";
import { EmployeeResponse } from "types/employee_schema";

export const useGetUser = () => {
  const [employee, setEmployee] = useState<EmployeeResponse>({
    names: "",
    lastnames: "",
    documentNumber: "",
    role: ""
  });

  useEffect(() => {
    axios
      .get(`${environment.dev_uri}/account/user`, {
        headers: {
          Authorization: `${localStorage.getItem("authToken")}`,
        },
      })
      .then(({ data }) => {
        setEmployee(data.employee);
      })
      .catch((error) => error);
  }, []);

  return employee;
};
