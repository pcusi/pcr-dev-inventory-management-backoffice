import { useState } from "react";

export const useDrawerEventAction = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [sideWidth, setSideWidth] = useState<string>("0");

  const handleDrawerToggle = () => {
    if (!isOpen) {
      setIsOpen(true);
      setTimeout(() => setSideWidth("30%"), 100);
    } else {
      setSideWidth("0");
      setTimeout(() => setIsOpen(false), 400);
    }
  };

  return { isOpen, sideWidth, handleDrawerToggle };
};
