import axios from "axios"
import React, { useEffect } from "react";
import { DashboardResponse } from "types/dashboard_schema";
import { environment } from "../environment/environment";

export const useGetDashboardReports = () => {
    const [reports, setReports] = React.useState<DashboardResponse>({
        providers: {
            title: "",
            count: 0,
        },
        customers: {
            title: "",
            count: 0,
        },
        kardex_entry: {
            title: "",
            count: 0,
        },
        kardex_exit: {
            title: "",
            count: 0
        }
    });

    useEffect(() => {
        axios.get(`${environment.dev_uri}/dashboard/report`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `${localStorage.getItem("authToken")}`
            }
        }).then(({data}) => {
            setReports(data.report);
        }).catch((error) => error);
    }, []);

    return reports;
}