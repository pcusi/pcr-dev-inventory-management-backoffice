import { useEffect, useRef, useState } from "react";
import { getEmployees } from "src/helpers/employee/getEmployee";
import { EmployeeResponse } from "types/employee_schema";

export const useGetEmployees = () => {
  const [employees, setEmployees] = useState<EmployeeResponse[]>([]);
  const isMounted = useRef(false);

  const getEmployeesData = async () => {
    const employees = await getEmployees();
    setEmployees(employees);
  };

  useEffect(() => {
    isMounted.current = true;

    if (isMounted.current) getEmployeesData();

    return () => {
      isMounted.current = false;
    };
  }, []);

  return { employees, setEmployees };
};
