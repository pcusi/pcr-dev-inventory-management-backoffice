import { useState } from "react";

export const usePagination = (length: number) => {
  let [currentPage, setCurrentPage] = useState(0);
  const [dataPerPage, setDataPerPage] = useState(5);
  let [offset, setOffset] = useState(0);

  const totalPages = Math.ceil(length / dataPerPage - 1);
  
  const nextPage = () => {
    if (currentPage < totalPages) currentPage++;
    setCurrentPage(currentPage);
    setOffset(currentPage * dataPerPage);
  };

  const prevPage = () => {
    if (currentPage >= 1) currentPage--;
    setCurrentPage(currentPage);
    setOffset(currentPage * dataPerPage);
  };

  return {
    currentPage,
    setCurrentPage,
    dataPerPage,
    setDataPerPage,
    totalPages,
    nextPage,
    prevPage,
    offset,
  };
};
