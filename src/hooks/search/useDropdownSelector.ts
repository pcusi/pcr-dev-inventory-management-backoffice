import { useState } from "react";

export const useDropdownSelector = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selected, setSelected] = useState("");

  const openSelect = () => {
    setIsOpen(!isOpen);
  };

  const selectedChange = (value: string) => {
    setSelected(value);
    setIsOpen(!isOpen);
    return value;
  };

  const resetChange = () => {
    setSelected("");
  };

  return {
    isOpen,
    openSelect,
    selectedChange,
    selected,
    setSelected,
    resetChange,
  };
};
