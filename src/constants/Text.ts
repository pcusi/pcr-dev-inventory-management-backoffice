import { FontFamilyEnum, fontStyle, FontWeightEnum } from "src/infrastructure/FontFamilyEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";

export const kTextThumb = {
  fontFamily: fontStyle("Inter", FontFamilyEnum.MEDIUM),
  fontWeight: FontWeightEnum.MEDIUM,
  fontSize: ".875rem",
  fontColor: TextColorEnum.PRIMARY_60
};

export const kTitleCollapse = {
  fontFamily: fontStyle("Inter", FontFamilyEnum.MEDIUM),
  fontWeight: FontWeightEnum.MEDIUM,
  fontSize: "1rem",
  fontColor: TextColorEnum.PRIMARY_60
}

export const kSubtitleCollapse = {
  fontFamily: fontStyle("Inter", FontFamilyEnum.REGULAR),
  fontWeight: FontWeightEnum.REGULAR,
  fontSize: ".875rem",
  fontColor: TextColorEnum.PRIMARY_40
}