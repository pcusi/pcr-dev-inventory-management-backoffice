import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import {
  fontStyle,
  FontFamilyEnum,
  FontWeightEnum,
} from "src/infrastructure/FontFamilyEnum";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";

export const kTable = {
  fontFamily: fontStyle("Inter", FontFamilyEnum.THIN),
  fontWeight: FontWeightEnum.THIN,
  fontColor: TextColorEnum.PRIMARY_60,
  fontSize: ".875rem",
  textTransform: "uppercase",
  gap: "12px",
  headerPadding: paddingREM(16, 16, 16, 16),
  headerBackground: BackgroundLightEnum.LIGHT_20,
  bodyPadding: paddingREM(16, 16, 16, 16),
  borderTop: BorderColorEnum.GRAY,
  borderBottom: BorderColorEnum.GRAY,
};
