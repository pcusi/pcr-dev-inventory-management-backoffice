import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { BoxShadowEnum } from "src/infrastructure/BoxShadowEnum";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";

export const kDrawerHeader = {
  height: "78px",
  background: BackgroundLightEnum.LIGHT_GRAY,
  boxShadow: BoxShadowEnum.SHADOW_GRAY,
  padding: paddingREM(32, 32, 32, 32),
  isFlex: true,
  isFlexRow: true,
  alignItems: "center",
};

export const kDrawerFooter = {
  position: "absolute",
  height: "78px",
  background: BackgroundLightEnum.LIGHT_GRAY,
  boxShadow: BoxShadowEnum.SHADOW_GRAY,
  padding: paddingREM(32, 32, 32, 32),
  isFlex: true,
  isFlexRow: true,
  alignItems: "center",
};
