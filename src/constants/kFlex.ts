export const kRowBetween = {
    isFlex: true,
    isFlexRow: true,
    justifyContent: 'space-between',
}

export const kRowCenter = {
    isFlex: true,
    isFlexRow: true,
    alignItems: 'center',
}

export const kRowJustifyCenter = {
    isFlex: true,
    isFlexRow: true,
    alignItems: 'center',
    justifyContent: 'center'
}