import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import {
  fontStyle,
  FontFamilyEnum,
  FontWeightEnum,
} from "src/infrastructure/FontFamilyEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";

export const kInputMaterial = {
  background: BackgroundLightEnum.LIGHT,
  fontSize: "0.875rem",
  fontFamily: fontStyle("Inter", FontFamilyEnum.LIGHT),
  fontWeight: FontWeightEnum.LIGHT,
  fontColor: TextColorEnum.PRIMARY_40,
};

export const kInputSearch = {
  background: BackgroundLightEnum.LIGHT,
  fontSize: "0.875rem",
  fontFamily: fontStyle("Inter", FontFamilyEnum.LIGHT),
  fontWeight: FontWeightEnum.LIGHT,
  fontColor: TextColorEnum.PRIMARY_40,
  borderColor: BorderColorEnum.GRAY,
  width: "100%"
};

export const kInputSelect = {
  background: BackgroundLightEnum.LIGHT,
  fontSize: "0.875rem",
  fontFamily: fontStyle("Inter", FontFamilyEnum.LIGHT),
  fontWeight: FontWeightEnum.LIGHT,
  fontColor: TextColorEnum.PRIMARY_40,
  borderColor: BorderColorEnum.GRAY,
};