import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "src/infrastructure/FontFamilyEnum";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";

export const kButtonPrimary = {
  background: BackgroundLightEnum.PRIMARY_20,
  backgroundHover: BackgroundLightEnum.PRIMARY,
  borderRadius: 8,
  fontSize: ".875rem",
  fontFamily: fontStyle("Inter", FontFamilyEnum.REGULAR),
  fontWeight: FontWeightEnum.REGULAR,
  fontColor: TextColorEnum.WHITE,
  padding: paddingREM(12, 16, 12, 16),
};

export const kButtonCancel = {
  background: "none",
  backgroundHover: BackgroundLightEnum.LIGHT_20,
  borderRadius: 8,
  fontSize: ".875rem",
  fontFamily: fontStyle("Inter", FontFamilyEnum.REGULAR),
  fontWeight: FontWeightEnum.REGULAR,
  fontColor: TextColorEnum.PRIMARY_40,
  padding: paddingREM(16, 16, 16, 16),
};

export const kButtonPagination = {
  background: "none",
  borderRadius: 8,
  style: {
    marginBottom: "1rem",
  }
}