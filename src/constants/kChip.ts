import { fontStyle, FontFamilyEnum } from "src/infrastructure/FontFamilyEnum";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";

export const roleChipColor = (role: string) => {
  switch (role) {
    case "Administradora":
      return {
        background: "rgb(255, 25, 67, .1)",
        fontColor: "rgb(255, 25, 67)",
        padding: paddingREM(4, 8, 4, 8),
        fontFamily: fontStyle("Inter", FontFamilyEnum.THIN),
        fontSize: ".75rem",
        borderRadius: 12,
        value: role,
      };
    case "Operador":
      return {
        background: "rgb(51, 194, 255, .1)",
        fontColor: "rgba(51, 194, 255)",
        padding: paddingREM(4, 8, 4, 8),
        fontFamily: fontStyle("Inter", FontFamilyEnum.THIN),
        fontSize: ".75rem",
        borderRadius: 12,
        value: role,
      };
  }
};
