import axios from "axios";
import { environment } from "src/environment/environment";

export const getEmployees = async () => {
  try {
    const response = await axios.get(`${environment.dev_uri}/employees`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `${localStorage.getItem("authToken")}`,
      },
    });

    return response.data.employees;
  } catch (e) {
    throw e;
  }
};
