import styled from "styled-components";
import { FC, useMemo } from "react";
import { ITable } from "../../infrastructure/interfaces/ITable";
import { Container } from "../container/Container";
import { paddingREM } from "../../infrastructure/PaddingConvertEnum";
import { SearchComponent } from "../searchbox/Searchbox";
import { ISearchbox } from "../../infrastructure/interfaces/ISearchbox";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import { PaginationComponent } from "../admin/pagination/PaginationComponent";
import { Chips } from "../chips/Chips";
import { roleChipColor } from "src/constants/kChip";
import { Thumbnail, Text, Button } from "..";
import { kTextThumb } from "src/constants/Text";
import { marginREM } from "src/infrastructure/MarginConvertEnum";
import { kRowCenter } from "src/constants/kFlex";
import { IoCreate, IoTrash } from "react-icons/io5";

const Table = styled.table<ITable>`
  width: 100%;
  border-collapse: collapse;

  thead {
    width: ${(props) => (props.width ? `${props.width}px` : "auto")};
    tr {
      background: ${(props) =>
        props.headerBackground ? `${props.headerBackground}` : "transparent"};
      border-top: ${(props) =>
        props.borderTop ? `1px solid ${props.borderTop}` : "none"};
      border-bottom: ${(props) =>
        props.borderBottom ? `1px solid ${props.borderBottom}` : "none"};
      th {
        padding: ${(props) =>
          props.headerPadding ? `${props.headerPadding}` : "0"};
        font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
        font-family: ${(props) =>
            props.fontFamily ? `${props.fontFamily}` : "Arial"},
          sans-serif;
        font-weight: ${(props) =>
          props.headerWeight ? props.headerWeight : 400};
        text-align: ${(props) => (props.textAlign ? props.textAlign : "left")};
        color: ${(props) => (props.fontColor ? props.fontColor : "black")};
        margin: ${(props) => (props.margin ? props.margin : "0")};
        text-transform: ${(props) =>
          props.textTransform ? props.textTransform : "inherit"};
      }
    }
  }

  tbody {
    tr {
      background: ${(props) =>
        props.bodyBackground ? `${props.bodyBackground}` : "transparent"};
      border-top: ${(props) =>
        props.borderTop ? `1px solid ${props.borderTop}` : "none"};
      border-bottom: ${(props) =>
        props.borderBottom ? `1px solid ${props.borderBottom}` : "none"};
      td {
        padding: ${(props) =>
          props.bodyPadding ? `${props.bodyPadding}` : "0"};
        width: ${(props) => (props.width ? `${props.width}px` : "auto")};
        font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
        font-family: ${(props) =>
            props.fontFamily ? `${props.fontFamily}` : "Arial"},
          sans-serif;
        font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
        text-align: ${(props) =>
          props.textAlign ? props.textAlign : "normal"};
        color: ${(props) => (props.fontColor ? props.fontColor : "black")};
        margin: ${(props) => (props.margin ? props.margin : "0")};
      }
    }
  }
`;

export const TableComponent: FC<ITable & ISearchbox> = (props) => {
  const searchData = useMemo(() => {
    return props.data
      ?.slice(props.offset!, props.offset! + props.rowsPerPage!)
      .filter((response: any) =>
        response[props.keyObject!].toLowerCase().includes(props.value)
      );
  }, [props.data, props.keyObject, props.value, props.offset, props.rowsPerPage]);

  return (
    <>
      <Container padding={paddingREM(16, 16, 16, 16)}>
        <SearchComponent
          gap={props.gap}
          onChange={props.onChange}
          value={props.value}
          border={BorderColorEnum.GRAY}
          borderRadius={12}
          padding={paddingREM(8, 16, 8, 16)}
          placeholder={props.placeholder}
          keyObject={props.keyObject}
        />
      </Container>
      <Container width="100%" padding={paddingREM(0, 0, 8, 0)}>
        <Table
          fontFamily={props.fontFamily}
          fontWeight={props.fontWeight}
          fontSize={props.fontSize}
          fontColor={props.fontColor}
          textAlign={props.textAlign}
          textTransform={props.textTransform}
          headerPadding={props.headerPadding}
          headerWeight={props.headerWeight}
          headerBackground={props.headerBackground}
          bodyPadding={props.bodyPadding}
          bodyWeight={props.bodyWeight}
          bodyBackground={props.bodyBackground}
          borderTop={props.borderTop}
          borderBottom={props.borderBottom}
        >
          <thead>
            <tr>
              {props.columns?.map(({ header }) => (
                <th key={Math.random()}>{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {searchData!.map((response) => (
              <TableRow
                key={Math.random()}
                response={response}
                columns={props.columns}
              ></TableRow>
            ))}
          </tbody>
        </Table>
      </Container>
      <Container padding={paddingREM(0, 16, 8, 16)}>
        <PaginationComponent
          totalPages={props.totalPages}
          totalRows={props.totalRows}
          rowsPerPage={props.rowsPerPage}
          setDataPerPage={props.setDataPerPage}
          currentPage={props.currentPage}
          nextPage={props.nextPage}
          prevPage={props.prevPage}
          isOpen={props.isOpen}
          openSelect={props.openSelect}
          selectedChange={props.selectedChange}
          resetChange={props.resetChange}
          selected={props.selected}
          options={props.options}
        ></PaginationComponent>
      </Container>
    </>
  );
};

const TableRow: FC<ITable> = (props) => {
  return (
    <tr>
      {props.columns!.map(({ value }) => {
        return renderColumnStyle(value!, props.response);
      })}
    </tr>
  );
};

const renderColumnStyle = (value: string, response: any) => {
  switch (value) {
    case "role":
      return (
        <td key={Math.random()}>
          <Chips
            {...roleChipColor(response[`${value}`])}
            value={response[`${value}`]}
          ></Chips>
        </td>
      );
    case "names":
      return (
        <td key={Math.random()}>
          <Thumbnail>
            <Text {...kTextThumb} margin={marginREM(0, 0, 8, 0)}>
              {response["names"]} {response["lastnames"]}
            </Text>
            <Chips
              {...roleChipColor(response["role"])}
              value={response["role"]}
            ></Chips>
          </Thumbnail>
        </td>
      );
    case "id":
      return (
        <td key={Math.random()}>
          <Container {...kRowCenter} gap="12px">
            <Button>
              <IoCreate></IoCreate>
            </Button>
            <Button>
              <IoTrash></IoTrash>
            </Button>
          </Container>
        </td>
      );
    default:
      return <td key={Math.random()}>{response[`${value}`]}</td>;
  }
};
