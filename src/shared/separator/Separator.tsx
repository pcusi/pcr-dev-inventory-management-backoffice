import { FC } from "react";
import { IContainer } from "src/infrastructure/interfaces/IContainer";
import { Container } from "../container/Container";

export const Separator: FC<IContainer> = (props) => {
  return (
    <>
      <Container
        width={props.width}
        height={props.height}
        background={props.background}
        margin={props.margin}
      ></Container>
    </>
  );
};
