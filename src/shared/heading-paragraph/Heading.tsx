import styled from "styled-components";
import { IHeading } from "../../infrastructure/interfaces/IHeading";

export const Heading = styled.h1<IHeading>`
  width: ${(props) => (props.width ? `${props.width}px` : "auto")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  line-height: 1.2;
`;

export const Text = styled.p<IHeading>`
  display: inline;
  span {
    background-color: ${(props) =>
      props.background ? `${props.background}` : "transparent"};
    padding: ${(props) => (props.padding ? `${props.padding}` : "0")};
    border-radius: ${(props) =>
      props.borderRadius ? `${props.borderRadius}px` : "0"};
  }
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  line-height: 1.2;
`;

export const Label = styled.label<IHeading>`
  width: ${(props) => (props.width ? `${props.width}px` : "auto")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  line-height: 1.2;
`;

export const Anchor = styled.a<IHeading>`
  width: ${(props) => (props.width ? `${props.width}px` : "auto")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  cursor: pointer;
  line-height: 1.2;
`;
