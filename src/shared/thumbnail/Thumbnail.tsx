import React, { FC } from "react";
import { AvatarEnum } from "src/infrastructure/AvatarEnum";
import { IHeading } from "src/infrastructure/interfaces/IHeading";
import { Container } from "..";

export const Thumbnail: FC<IHeading> = (props) => {
  return (
    <Container isFlex={true} isFlexRow={true} width="100%" gap="12px" alignItems="center">
      <Container
        width={AvatarEnum.WIDTH_M}
        height={AvatarEnum.HEIGHT_M}
        borderRadius={999}
        background="red"
      ></Container>
      <Container isFlex={true} isFlexRow={false}>
        {props.children}
      </Container>
    </Container>
  );
};
