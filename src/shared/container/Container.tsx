import styled from "styled-components";
import { ICard } from "../../infrastructure/interfaces/ICard";

export const Container = styled.div<ICard>`
  position: ${(props) => (props.position ? props.position : "relative")};
  width: ${(props) => (props.width ? props.width : "auto")};
  height: ${(props) => (props.height ? `${props.height}` : "auto")};
  background: ${(props) =>
    props.background ? props.background : "none"};
  ${(props) => (props.isFlex ? "display: flex" : null)};
  ${(props) =>
    props.isFlexRow ? "flex-direction: row" : "flex-direction: column"};
  ${(props) => (props.isFlexGrow ? "flex-grow: 1" : null)};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  padding: ${(props) => (props.padding ? `${props.padding}` : "0")};
  ${(props) =>
    props.alignItems
      ? `
    align-items: ${props.alignItems};
  `
      : null};
  ${(props) =>
    props.justifyContent
      ? `
    justify-content: ${props.justifyContent};
  `
      : null};
  gap: ${(props) => (props.gap ? `${props.gap}` : "0")};
  box-shadow: ${(props) => (props.boxShadow ? `${props.boxShadow}` : null)};
  border-radius: ${(props) =>
    props.borderRadius ? `${props.borderRadius}px` : "0"};
  cursor: ${(props) => (props.cursor ? props.cursor : "")};
  ${(props) =>
    props.hasBlur
      ? `
      backdrop-filter: blur(10px);
      -webkit-backdrop-filter: blur(10px);
  `
      : null};
`;
