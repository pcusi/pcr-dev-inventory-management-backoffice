import React, { FC } from "react";
import { IoChevronBack, IoChevronForward } from "react-icons/io5";
import { kInputSelect } from "src/constants/Input";
import { kRowBetween, kRowCenter } from "src/constants/kFlex";
import { kSubtitleCollapse } from "src/constants/Text";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import { IPagination } from "src/infrastructure/interfaces/components/IPagination";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";
import { ButtonIcon, Container, Text } from "src/shared";
import { InputSelect } from "src/shared/input/Select";

export const PaginationComponent: FC<IPagination> = (props) => {
  return (
    <>
      <Container {...kRowBetween}>
        <div></div>
        <Container {...kRowCenter} gap="20px">
          <Text {...kSubtitleCollapse}>Filas por página:</Text>
          <InputSelect
            {...kInputSelect}
            isOpen={props.isOpen}
            textLabel={props.selected}
            openSelect={props.openSelect}
            selectedChange={props.selectedChange}
            resetChange={props.resetChange}
            selected={props.selected}
            padding={paddingREM(16, 16, 16, 16)}
            options={props.options}
            focusBorderColor={BorderColorEnum.PRIMARY}
          />
          <Text {...kSubtitleCollapse}>
            1-{props.rowsPerPage} de {props.totalRows}
          </Text>
          <Container {...kRowCenter} gap="4px">
            <ButtonIcon onClick={props.prevPage}> 
              <IoChevronBack></IoChevronBack>
            </ButtonIcon>
            <ButtonIcon onClick={props.nextPage} disabled={props.currentPage === props.totalPages}>
              <IoChevronForward></IoChevronForward>
            </ButtonIcon>
          </Container>
        </Container>
      </Container>
    </>
  );
};
