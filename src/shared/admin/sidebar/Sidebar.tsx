import { IEmployee } from "src/infrastructure/interfaces/components/IEmployee";
import styled from "styled-components";
import { IContainer } from "src/infrastructure/interfaces/IContainer";

export const Sidebar = styled.div<IContainer & IEmployee>`
  position: fixed;
  top: 0;
  left: 0;
  width: ${(props) => (props.width ? props.width : "auto")};
  max-width: ${(props) => (props.maxWidth ? props.maxWidth : "auto")};
  background: ${(props) => (props.background ? props.background : "black")};
  height: 100%;
  padding: ${(props) => (props.padding ? props.padding : "0")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  transition: all 500ms;
`;
