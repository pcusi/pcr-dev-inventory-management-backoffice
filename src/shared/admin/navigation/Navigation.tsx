import { NavLink } from "react-router-dom";
import { NavigationRoute } from "../../../infrastructure/routes/NavigationRoute";
import styled from "styled-components";
import { INavigation } from "../../../infrastructure/interfaces/INavigation";
import { paddingREM } from "../../../infrastructure/PaddingConvertEnum";
import { TextColorEnum } from "../../../infrastructure/TextColorEnum";
import { pixelTOREM } from "../../../infrastructure/interfaces/IHeading";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "../../../infrastructure/FontFamilyEnum";
import { marginREM } from "src/infrastructure/MarginConvertEnum";

export const Navbar = styled.nav<INavigation>`
  list-style-type: none;
  margin: ${(props) => (props.margin ? props.margin : "0")};
`;

export const NavItem = styled.li<INavigation>`
  width: ${(props) => (props.width ? `${props.width}` : "auto")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  ${(props) => (props.isFlex ? "display: flex" : null)};
  ${(props) =>
    props.isFlexRow ? "flex-direction: row" : "flex-direction: column"};
  ${(props) =>
    props.alignItems
      ? `
    align-items: ${props.alignItems};
  `
      : null};
  ${(props) =>
    props.justifyContent
      ? `
    justify-content: ${props.justifyContent};
  `
      : null};
  .link {
    color: ${(props) => (props.fontColor ? props.fontColor : "white")};
    text-decoration: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    gap: ${(props) => (props.gap ? `${props.gap}` : "0")};
    padding: .75rem 1rem;
    font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
    .icon {
      font-size: ${(props) => (props.iconSize ? `${props.iconSize}` : "1rem")};
      color: ${(props) => (props.iconColor ? props.iconColor : "white")};
      width: 20px;
      height: 20px;
    }
  }
  .active {
    width: 100%;
    color: ${(props) => (props.fontColor ? props.fontColor : "white")};
    text-decoration: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    border-radius: 8px;
    background: rgba(255, 255, 255, 0.06);
    padding: .75rem 1rem;
    gap: ${(props) => (props.gap ? `${props.gap}` : "0")};
    font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
    color: ${(props) =>
      props.fontActiveColor ? props.fontActiveColor : "white"};
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
    .icon {
      font-size: ${(props) => (props.iconSize ? `${props.iconSize}` : "1rem")};
      color: ${(props) =>
        props.iconActiveColor ? props.iconActiveColor : "white"};
      width: 20px;
      height: 20px;
    }
  }
`;

export const Navigation = ({
  navigation,
}: {
  navigation: NavigationRoute[];
}) => {
  return (
    <Navbar>
      {navigation.map(({ id, to, name, Icon }) => (
        <NavItem
          width="100%"
          key={id}
          padding={paddingREM(12, 12, 12, 12)}
          color={TextColorEnum.GRAY}
          gap="12px"
          iconSize={pixelTOREM(20)}
          fontFamily={fontStyle("Inter", FontFamilyEnum.REGULAR)}
          fontWeight={FontWeightEnum.REGULAR}
          fontColor={TextColorEnum.GRAY}
          fontSize=".875rem"
          fontActiveColor="white"
          iconActiveColor="white"
          margin={marginREM(0, 0, 8, 0)}
        >
          <NavLink
            to={to}
            className={({ isActive }) => (isActive ? "active" : "link")}
          >
            <Icon className="icon"></Icon> {name}
          </NavLink>
        </NavItem>
      ))}
    </Navbar>
  );
};
