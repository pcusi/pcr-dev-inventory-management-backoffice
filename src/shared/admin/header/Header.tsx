import { Container } from "../../container/Container";
import { FC } from "react";
import { ICard } from "../../../infrastructure/interfaces/ICard";

export const Header: FC<ICard> = (props) => {
  return (
    <Container
      {...props}
    >
      {props.children}
    </Container>
  );
};
