import React, { FC } from "react";
import { IContainer } from "src/infrastructure/interfaces/IContainer";
import { IHeader } from "src/infrastructure/interfaces/IHeader";
import { Container } from "src/shared";

export const DrawerFooter: FC<IContainer & IHeader> = (props) => {
  return (
    <Container width="100%" {...props}>
      {props.children}
    </Container>
  );
};
