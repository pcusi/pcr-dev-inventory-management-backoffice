import React, { FC } from "react";
import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { BoxShadowEnum } from "src/infrastructure/BoxShadowEnum";
import { IDrawer } from "src/infrastructure/interfaces/components/IDrawer";
import { Container } from "src/shared";

export const Drawer: FC<IDrawer> = (props) => {
  return (
    <>
      <Container
        position="fixed"
        width="100%"
        height="100%"
        background={BackgroundLightEnum.DARK_OPACITY}
        boxShadow={BoxShadowEnum.SHADOW_GRAY}
        isFlex={true}
        isFlexRow={true}
        style={{
          top: 0,
          left: 0,
          zIndex: 999,
          transition: "0.3s linear",
        }}
      >
        <Container
          width="70%"
          height="100%"
          onClick={props.onClick}
          style={{
            left: 0,
          }}
        ></Container>
        <Container
          position="absolute"
          width="40%"
          height="100%"
          background={BackgroundLightEnum.LIGHT}
          boxShadow={BoxShadowEnum.SHADOW_GRAY}
          style={{
            transition: "0.3s linear",
            right: props.sideWidth === "30%" ? 0 : "-850px",
          }}
        >
          {props.children}
        </Container>
      </Container>
    </>
  );
};
