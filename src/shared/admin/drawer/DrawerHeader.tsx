import React, { FC } from "react";
import { IContainer } from "src/infrastructure/interfaces/IContainer";
import { IHeader } from "src/infrastructure/interfaces/IHeader";
import { Container } from "src/shared";

export const DrawerHeader: FC<IContainer & IHeader> = (props) => {
  return (
    <>
      <Container
        {...props}
      >
        <Container isFlex={true}>
          {props.children}
        </Container>
      </Container>
    </>
  );
};
