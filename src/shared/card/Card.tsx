import styled from "styled-components";
import { ICard } from "../../infrastructure/interfaces/ICard";

export const Card = styled.div<ICard>`
  width: ${(props) => (props.width ? props.width : "auto")};
  height: ${(props) => (props.height ? `${props.height}` : "auto")};
  background: ${(props) => (props.background ? props.background : "#fff")};
  border-radius: ${(props) =>
    props.borderRadius ? `${props.borderRadius}px` : "0"};
  ${(props) => (props.isFlex ? "display: flex" : null)};
  ${(props) =>
    props.isFlexRow ? "flex-direction: row" : "flex-direction: column"};
  ${(props) => (props.isFlexGrow ? "flex-grow: 1" : null)};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  padding: ${(props) => (props.padding ? `${props.padding}` : "0")};
  ${(props) =>
    props.alignItems
      ? `
    align-items: ${props.alignItems};
  `
      : null};
  box-shadow: 0px 4px 50px rgba(192, 192, 192, 0.25);
`;
