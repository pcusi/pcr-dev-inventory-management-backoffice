import styled from "styled-components";
import { FC } from "react";
import { ICheckbox } from "../../infrastructure/interfaces/ICheckbox";
import { Container } from "../container/Container";
import { Label } from "../heading-paragraph/Heading";

const Checkbox = styled.input<ICheckbox>`
  width: ${(props) => (props.width ? `${props.width}` : "100%")};
  height: ${(props) => (props.height ? `${props.height}` : "100%")};
  position: relative;
  border-radius: 8px;
  margin: ${(props) => (props.margin ? props.margin : "0")};
  cursor: pointer;
`;

export const CheckboxMaterial: FC<ICheckbox> = (props) => {
  return (
    <>
      <Container
        width={props.width}
        isFlex={true}
        isFlexRow={true}
        justifyContent="space-between"
        alignItems="center"
      >
        <Checkbox type="checkbox" width="15px" height="15px"></Checkbox>
        <Label
          fontSize="0.875rem"
          fontFamily={props.fontFamily}
          fontWeight={props.fontWeight}
          fontColor={props.fontColor}
        >
          {props.value}
        </Label>
      </Container>
    </>
  );
};
