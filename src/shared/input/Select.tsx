import { FC } from "react";
import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import { BoxShadowEnum } from "src/infrastructure/BoxShadowEnum";
import { IInput } from "src/infrastructure/interfaces/IInput";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";
import styled from "styled-components";
import { Button, ButtonIcon } from "../button";
import { IoCaretDown, IoCaretUp, IoClose } from "react-icons/io5";
import { Container } from "../container/Container";
import { kRowCenter } from "src/constants/kFlex";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";


const DropdownHeader = styled.div<IInput>`
  background: transparent;
  height: ${(props) => (props.height ? props.height : "auto")};
  ${(props) =>
    props.isMaterial
      ? `
    border: 2px solid ${BorderColorEnum.GRAY};
  `
      : null};
  border-radius: 8px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 1rem 0.5rem;
  cursor: pointer;
  span {
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    line-height: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    color: ${TextColorEnum.PRIMARY_40};
  }
`;

const DropdownContainer = styled.div<IInput>`
  position: relative;
  background: ${(props) => (props.background ? props.background : "#fff")};
  border-radius: 8px;
  border: none;
  height: ${(props) => (props.height ? props.height : "auto")};
`;

const DropdownList = styled.ul`
  position: absolute;
  top: 0;
  left: 0;
  background: ${BackgroundLightEnum.LIGHT};
  border-radius: 8px;
  box-shadow: ${BoxShadowEnum.SHADOW_GRAY};
  list-style-type: none;
`;

const DropdownItem = styled.li<IInput>`
  margin: 0;
  width: 100%;
  &:hover {
    background: ${BackgroundLightEnum.LIGHT_GRAY};
  }
  padding: 0.25rem 0.75rem;
  &:first-of-type {
    border-radius-top-left: 8px;
    border-radius-top-right: 8px;
  }
  &:last-of-type {
    border-radius-bottom-left: 8px;
    border-radius-bottom-right: 8px;
  }
  span {
    width: 100%;
    text-decoration: none;
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    line-height: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    color: ${TextColorEnum.PRIMARY_40};
  }
`;

export const InputSelect: FC<IInput> = (props) => {
  return (
    <DropdownContainer {...props}>
      <DropdownHeader {...props}>
        <span>{props.selected}</span>
        <Container {...kRowCenter} gap="0.5rem">
          <Container
            background={BackgroundLightEnum.RED_OPACITY}
            borderRadius={6}
            padding={paddingREM(6, 6, 6, 6)}
            hidden={!props.isMaterial}
          >
            <Button
              type="button"
              onClick={props.resetChange}
              background="none"
              backgroundHover="none"
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <IoClose
                style={{
                  color: TextColorEnum.ERROR,
                }}
              ></IoClose>
            </Button>
          </Container>
          <ButtonIcon type="button" onClick={props.openSelect}>
            {props.isOpen ? <IoCaretUp /> : <IoCaretDown />}
          </ButtonIcon>
        </Container>
      </DropdownHeader>
      <DropdownList hidden={!props.isOpen}>
        {props.options?.map((option) => (
          <DropdownItem
            key={Math.random()}
            onClick={() => props.selectedChange!(option)}
          >
            <span>{option}</span>
          </DropdownItem>
        ))}
      </DropdownList>
    </DropdownContainer>
  );
};
