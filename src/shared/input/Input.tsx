import styled from "styled-components";
import { IInput } from "../../infrastructure/interfaces/IInput";
import React from "react";

const MaterialField = styled.div<IInput>`
  width: ${(props) => (props.width ? `${props.width}` : "100%")};
  height: auto;
  position: relative;
  border: 2px solid ${(props) => props.borderColor};
  border-radius: 8px;
  margin: ${(props) => (props.margin ? props.margin : "0")};
  .mat-form-field {
    padding: 1rem .5rem;
    border: none;
    background-color: transparent;
    width: 100%;
    color: ${(props) => (props.fontColor ? props.fontColor : "black")};
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    line-height: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    &:focus {
      outline: none;
    }
    &::placeholder {
      color: black;
    }
  }
  &:hover,
  &:focus-within {
    border-color: ${(props) => props.focusBorderColor};
  }
  & .mat-form-field:focus,
  &:hover .mat-form-field {
    color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  }
  label {
    position: absolute;
    background-color: transparent;
    padding: 1rem .5rem;
    font-family: ${(props) =>
        props.fontFamily ? `${props.fontFamily}` : "Arial"},
      sans-serif;
    font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
    line-height: ${(props) => (props.fontSize ? `${props.fontSize}` : "1rem")};
    transition: all 150ms;
    top: 0;
    left: 0;
    bottom: 0;
    color: ${(props) => (props.fontColor ? props.fontColor : "black")};
    cursor: text;
  }
  &:hover label {
    color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  }
  &:focus-within label,
  & .mat-form-field:not(:placeholder-shown) ~ label {
    padding: 0px 8px;
    font-size: ${(props) =>
      props.fontSize ? `calc(${props.fontSize} - 4px)` : "1rem"};
    background: ${(props) => (props.background ? props.background : "#fff")};
    top: -8px;
    left: 8px;
    bottom: auto;
    color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  }
`;

export const InputMaterial = React.forwardRef<HTMLInputElement, IInput>(
  (props, ref) => (
    <>
      <MaterialField {...props}>
        <input
          className="mat-form-field"
          placeholder=" "
          type={props.type}
          ref={ref}
          {...props.register}
        />
        <label>{props.textLabel}</label>
      </MaterialField>
    </>
  )
);
