import styled from "styled-components";
import React from "react";
import { Container } from "../container/Container";
import { IErrorForm } from "../../infrastructure/interfaces/IErrorForm";
import { IErrorMessage } from "../../infrastructure/interfaces/IErrorMessage";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "../../infrastructure/FontFamilyEnum";
import { marginREM } from "../../infrastructure/MarginConvertEnum";
import { TextColorEnum } from "../../infrastructure/TextColorEnum";

const Span = styled.span<IErrorForm>`
  width: ${(props) => (props.width ? `${props.width}px` : "auto")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "inherit")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  margin: ${(props) => (props.margin ? props.margin : "0")};
  line-height: 1.2;
`;

const ErrorType = (error?: IErrorMessage | undefined) => {
  return (
    <Span
      fontSize="0.75rem"
      fontFamily={fontStyle("Inter", FontFamilyEnum.SEMI_BOLD)}
      fontWeight={FontWeightEnum.SEMI_BOLD}
      fontColor={TextColorEnum.ERROR}
      textAlign="left"
    >
      {error?.message}
    </Span>
  );
};

export const ErrorMessage: React.FC<IErrorForm> = (props) => {
  return (
    <>
      <Container margin={marginREM(0, 0, 12, 0)}
      alignItems="start" isFlex={true} isFlexRow={true} justifyContent="start">
        {ErrorType(props.error)}
      </Container>
    </>
  );
};
