import React, { FC } from "react";
import { IHeading } from "src/infrastructure/interfaces/IHeading";
import { Text } from "..";

export const Chips: FC<IHeading> = (props) => {
  return (
    <Text {...props}>
      <span>{props.value}</span>
    </Text>
  );
};
