import { BackgroundLightEnum } from 'src/infrastructure/BackgroundEnum';
import styled from 'styled-components'

export const ButtonIcon = styled.button`
    background: none;
    border: none;
    outline: none;
    padding: .25rem;
    &:hover {
        border-radius: 4px;
        background: ${BackgroundLightEnum.LIGHT_GRAY};
    }
    cursor: pointer;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;