import styled from "styled-components";
import { IButton } from "../../infrastructure/interfaces/IButton";

export const Button = styled.button<IButton>`
  width: ${(props) => (props.width ? props.width : "auto")};
  background: ${(props) => (props.background ? props.background : "#fff")};
  border-radius: ${(props) =>
    props.borderRadius ? `${props.borderRadius}px` : "0"};
  padding: ${(props) => (props.padding ? `${props.padding}` : "0")};
  font-size: ${(props) => (props.fontSize ? props.fontSize : "1rem")};
  font-family: ${(props) =>
      props.fontFamily ? `${props.fontFamily}` : "Arial"},
    sans-serif;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 400)};
  text-align: ${(props) => (props.textAlign ? props.textAlign : "normal")};
  color: ${(props) => (props.fontColor ? props.fontColor : "black")};
  line-height: 1.2;
  border: none;
  cursor: pointer;
  transition: all 300ms;
  &:hover {
    background: ${(props) =>
      props.backgroundHover ? props.backgroundHover : "transparent"};
  }
`;
