import styled from "styled-components";
import { ISpinner } from "../../infrastructure/interfaces/ISpinner";

export const Spinner = styled.div<ISpinner>`
  border: 4px solid rgba(0, 0, 0, 0.1);
  width: ${(props) => (props.width ? props.width : "0")};
  height: ${(props) => (props.height ? props.height : "0")};
  border-radius: 50%;
  border-left-color: ${(props) => (props.borderLeftColor ? props.borderLeftColor : "blue")};;
  animation: spin 1s ease infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }

    100% {
      transform: rotate(360deg);
    }
  }
`;
