import styled from "styled-components";
import { BsSearch } from "react-icons/bs";
import { ISearchbox } from "../../infrastructure/interfaces/ISearchbox";
import { FC } from "react";

const Searcher = styled.div<ISearchbox>`
  width: ${(props) => (props.width ? `${props.width}` : "auto")};
  height: ${(props) => (props.height ? `${props.height}` : "auto")};
  padding: ${(props) => (props.padding ? `${props.padding}` : "0")};
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: ${(props) => (props.gap ? `${props.gap}` : "auto")};
  border-radius: ${(props) =>
    props.borderRadius ? `${props.borderRadius}px` : "auto"};
  border: ${(props) => (props.border ? `1px solid ${props.border}` : "none")};
  input {
    border: none;
    width: 100%;
    &:focus {
      outline: none;
    }
  }
`;

export const SearchComponent: FC<ISearchbox> = (props) => {
  return (
    <>
      <Searcher
        gap={props.gap}
        border={props.border}
        borderRadius={props.borderRadius}
        padding={props.padding}
      >
        <BsSearch className="searcher-icon"></BsSearch>
        <input
          type="text"
          placeholder={props.placeholder}
          className="searcher"
          onChange={props.onChange}
          onFocus={props.onFocus}
          onBlur={props.onFocus}
          value={props.value}
        />
      </Searcher>
    </>
  );
};
