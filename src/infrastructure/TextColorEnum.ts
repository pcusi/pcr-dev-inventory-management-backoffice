export enum TextColorEnum {
  PRIMARY = "#2442AF",
  PRIMARY_20 = "#5568FF",
  PRIMARY_40 = "#646F87",
  PRIMARY_60 = "#233354",
  WHITE = "#fff",
  GRAY = "#C4C4C4",
  GRAY_20 = "#B9BAC0",
  ERROR = "#FE1943",
  LIGHT_GRAY = "#F3F5F9",
}
