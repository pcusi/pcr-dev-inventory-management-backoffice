export enum FontFamilyEnum {
    THIN = 'Thin',
    LIGHT = 'Light',
    REGULAR = 'Regular',
    MEDIUM = 'Medium',
    SEMI_BOLD = 'SemiBold',
    BOLD = 'Bold'
}

export enum FontWeightEnum {
    THIN = 100,
    LIGHT = 300,
    REGULAR = 400,
    MEDIUM = 500,
    SEMI_BOLD = 600,
    BOLD = 700
}

export function fontStyle(name: string, weight: string): string {
    const font_family: string = `${name}-${weight}`;
    return font_family;
}