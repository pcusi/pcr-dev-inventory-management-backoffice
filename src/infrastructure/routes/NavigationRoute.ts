import { LazyExoticComponent } from "react";
import { IconType } from "react-icons";

type JSXComponent = () => JSX.Element;

export interface NavigationRoute {
    id: number;
    name: string;
    to: string;
    Icon: IconType;
    path?: string;
    children?: NavigationRoute[];
    Component: LazyExoticComponent<JSXComponent> | JSXComponent;
}