export enum PixelRootEnum {
  PX_16 = 16,
}

export function marginREM(
  top: number,
  left: number,
  bottom: number,
  right: number
): string {
  const margin: string = `${top / PixelRootEnum.PX_16}rem ${
    left / PixelRootEnum.PX_16
  }rem ${bottom / PixelRootEnum.PX_16}rem ${right / PixelRootEnum.PX_16}rem`;

  return margin;
}
