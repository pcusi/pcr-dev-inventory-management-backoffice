export enum BorderColorEnum {
    GRAY = '#C4C4C4',
    PRIMARY = '#2442AF',
    ERROR = "#FE1943"
}