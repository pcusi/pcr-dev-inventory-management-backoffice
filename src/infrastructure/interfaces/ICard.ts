import { IContainer } from "./IContainer";
import { IHeader } from "./IHeader";

export interface ICard extends IContainer, IHeader {
  borderRadius?: number;
  cursor?: string;
}
