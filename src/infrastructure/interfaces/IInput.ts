import { IHeading } from "./IHeading";
import { UseFormRegisterReturn } from "react-hook-form";
import { ISelect } from "./components/ISelect";

export interface IInput extends IHeading, ISelect {
  focusBorderColor?: string;
  borderColor?: string;
  type?: string;
  textLabel?: string;
  register?: UseFormRegisterReturn;
}
