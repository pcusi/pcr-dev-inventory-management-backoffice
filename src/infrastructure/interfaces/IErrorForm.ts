import { IHeading } from "./IHeading";
import { IErrorMessage } from "./IErrorMessage";

export interface IErrorForm extends IHeading {
  error?: IErrorMessage;
}
