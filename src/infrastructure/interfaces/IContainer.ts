export interface IContainer {
    position?: string;
    width?: string;
    maxWidth?: string;
    minWidth?: string;
    height?: string;
    maxHeight?: string;
    background?: string;
    margin?: string;
    padding?: string;
    isFlex?: boolean;
    isFlexRow?: boolean;
    isFlexGrow?: boolean;
    alignItems?: string;
    justifyContent?: string;
    boxShadow?: string;
    hasBlur?: boolean;
    gap?: string;
}