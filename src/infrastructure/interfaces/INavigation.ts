import { IContainer } from "./IContainer";
import { IHeading } from "./IHeading";

export interface INavigation extends IContainer, IHeading {
    iconSize?: string;
    iconColor?: string;
    iconActiveColor?: string;
    fontActiveColor?: string;
}
