import { ICard } from "./ICard";
import { IHeading } from "./IHeading";
import React from "react";
import { IBorder } from "./IBorder";

export interface ISearchbox extends ICard, IHeading, IBorder {
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  onFocus?: React.FocusEventHandler | undefined;
  keyObject?: string | undefined;
  value?: string;
  placeholder?: string;
}
