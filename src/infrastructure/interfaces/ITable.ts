import { ICard } from "./ICard";
import { IHeading } from "./IHeading";
import { ReactNode } from "react";
import { IPagination } from "./components/IPagination";

export interface ITable extends IHeading, ICard, IPagination {
  columns?: ITableHeaders[];
  data?: object[];
  response?: any;
  children?: ReactNode;
  headerPadding?: string;
  headerBackground?: string;
  headerWeight?: string;
  bodyPadding?: string;
  bodyBackground?: string;
  bodyWeight?: string;
  borderTop?: string;
  borderBottom?: string;
}

export interface ITableHeaders {
  header?: string;
  value?: string;
}