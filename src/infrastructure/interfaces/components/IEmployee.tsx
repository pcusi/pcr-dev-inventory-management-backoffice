import { EmployeeResponse } from "types/employee_schema";

export interface IEmployee {
  employee?: EmployeeResponse;
  employees?: EmployeeResponse[];
  onClose?: () => void;
  setEmployees?: (employees: EmployeeResponse[]) => void;
  addEmployee?: (employees: EmployeeResponse[]) => void;
}