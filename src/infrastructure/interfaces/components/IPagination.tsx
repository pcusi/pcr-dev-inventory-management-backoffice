import { ISelect } from "./ISelect";

export interface IPagination extends ISelect {
    currentPage?: number;
    offset?: number;
    totalPages?: number;
    rowsPerPage?: number;
    totalRows?: number;
    setDataPerPage?: (value: string) => void;
    nextPage?: () => void;
    prevPage?: () => void;
}