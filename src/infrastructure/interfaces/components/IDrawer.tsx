export interface IDrawer {
    onClick?: () => void;
    isOpen?: boolean;
    sideWidth?: string;
}