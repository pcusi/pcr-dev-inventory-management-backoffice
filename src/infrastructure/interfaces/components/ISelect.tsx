export interface ISelect {
    isOpen?: boolean;
    isMaterial?: boolean;
    selected?: string;
    openSelect?: () => void;
    selectedChange?: (value: string) => void;
    resetChange?: () => void;
    options?: string[];
}