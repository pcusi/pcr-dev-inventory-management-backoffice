import { DashboardResponse } from "types/dashboard_schema";

export interface IDashboardReports {
    reports: DashboardResponse
}