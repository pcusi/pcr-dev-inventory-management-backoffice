import { CSSProperties } from "react";

export interface IHeader {
  style?: CSSProperties | undefined;
}
