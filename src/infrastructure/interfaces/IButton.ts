import {ICard} from "./ICard";
import { IHeading } from "./IHeading";

export interface IButton extends ICard, IHeading {
    type?: string;
    backgroundHover?: string;
}