import { PixelRootEnum } from "../MarginConvertEnum";
import { ICard } from "./ICard";

export interface IHeading extends ICard {
  fontFamily?: string;
  fontSize?: string;
  fontWeight?: number;
  fontColor?: string;
  textAlign?: string;
  textTransform?: string;
  value?: string;
}

export function pixelTOREM(pixel: number): string {
  const rem: string = `${pixel / PixelRootEnum.PX_16}rem`;

  return rem;
}
