import {IContainer} from "./IContainer";
import { IHeading } from "./IHeading";

export interface ICheckbox extends IContainer, IHeading {
    value?: string;
}