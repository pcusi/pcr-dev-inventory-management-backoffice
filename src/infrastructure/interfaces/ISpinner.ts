import { IContainer } from "./IContainer";

export interface ISpinner extends IContainer {
  borderLeftColor?: string;
}
