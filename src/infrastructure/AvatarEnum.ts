export enum AvatarEnum {
    WIDTH_M = "40px",
    HEIGHT_M = "40px",
    WIDTH_L = "50px",
    HEIGHT_L = "50px",
    WIDTH_XL = "60px",
    HEIGHT_XL = "60px"
}