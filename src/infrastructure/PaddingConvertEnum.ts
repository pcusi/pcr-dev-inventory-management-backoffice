import { PixelRootEnum } from "./MarginConvertEnum";

export function paddingREM(
  top: number,
  left: number,
  bottom: number,
  right: number
): string {
  const padding: string = `${top / PixelRootEnum.PX_16}rem ${
    left / PixelRootEnum.PX_16
  }rem ${bottom / PixelRootEnum.PX_16}rem ${right / PixelRootEnum.PX_16}rem`;

  return padding;
}
