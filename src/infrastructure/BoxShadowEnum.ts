export enum BoxShadowEnum {
    SHADOW_HEADER = "0px 4px 50px rgba(192, 192, 192, 0.25)",
    SHADOW_GRAY = "0px 2px 5px rgba(180, 181, 191, 0.31)"
}