import { Navigate, Route, Routes } from "react-router-dom";
import styled from "styled-components";
import { AuthComponent } from "./auth/AuthComponent";
import { AdminComponent } from "./admin/AdminComponent";

const Layout = styled.div`
  width: 100%;
  height: 100%;
`;

export const BackofficeLayout = () => {
  return (
    <>
      <Layout>
        <Routes>
          <Route key="auth" path="auth/*" element={<AuthComponent />} />
          <Route key="admin" path="admin/*" element={<AdminComponent />} />
          {localStorage.getItem("authToken") === null ? (
            <Route
              path="/"
              element={<Navigate to={"/auth/login"}></Navigate>}
            ></Route>
          ) : (
            <Route
              path="/"
              element={<Navigate to={"/admin/dashboard"}></Navigate>}
            ></Route>
          )}
        </Routes>
      </Layout>
    </>
  );
};
