import React, { FC } from "react";
import { useForm } from "react-hook-form";
import { kInputMaterial } from "src/constants/Input";
import { kSubtitleCollapse, kTitleCollapse } from "src/constants/Text";
import { BorderColorEnum } from "src/infrastructure/BorderColorEnum";
import { marginREM } from "src/infrastructure/MarginConvertEnum";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";
import { Button, Container, Heading, InputMaterial, Text } from "src/shared";
import { ErrorMessage } from "src/shared/errors/Errors";
import { EmployeeResponse } from "types/employee_schema";
import * as iErrors from "src/utils/json/errors.json";
import { DrawerHeader } from "src/shared/admin/drawer/DrawerHeader";
import { DrawerFooter } from "src/shared/admin/drawer/DrawerFooter";
import { kButtonCancel, kButtonPrimary } from "src/constants/Button";
import { kDrawerFooter, kDrawerHeader } from "src/constants/kDrawer";
import { Pattern } from "src/infrastructure/PatternEnum";
import axios from "axios";
import { environment } from "src/environment/environment";
import { IEmployee } from "src/infrastructure/interfaces/components/IEmployee";
import { getEmployees } from "src/helpers/employee/getEmployee";

export const CreateEmployeeComponent: FC<IEmployee> = (props) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<EmployeeResponse>({
    mode: "onChange",
    defaultValues: {
      names: "",
      lastnames: "",
      email: "",
      role: "Operador",
      password: "",
      documentNumber: "",
      documentType: "DNI",
    },
  });

  const error = JSON.parse(JSON.stringify(iErrors));

  const onSubmit = handleSubmit(async (data) => {
    await axios.post(`${environment.dev_uri}/employee`, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `${localStorage.getItem("authToken")}`,
      },
    });

    reset();

    const employees = await getEmployees();

    props.addEmployee!(employees);
  });

  return (
    <>
      <form onSubmit={onSubmit}>
        <DrawerHeader {...kDrawerHeader}>
          <Heading {...kTitleCollapse}>Agregar Empleado</Heading>
          <Text {...kSubtitleCollapse}>
            Agrega un nuevo empleado a tu empresa
          </Text>
        </DrawerHeader>
        <Container padding={paddingREM(32, 32, 32, 32)}>
          <Heading {...kTitleCollapse} margin={marginREM(0, 0, 24, 0)}>
            Información Básica
          </Heading>
          <InputMaterial
            type="text"
            textLabel="Nombres"
            {...kInputMaterial}
            focusBorderColor={
              errors.names ? BorderColorEnum.ERROR : BorderColorEnum.PRIMARY
            }
            borderColor={
              errors.names ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
            }
            register={register("names", {
              required: {
                value: true,
                message: error.EMPLOYEE.ERRORS.names.required,
              },
              min: {
                value: 3,
                message: error.EMPLOYEE.ERRORS.names.min,
              },
            })}
            margin={marginREM(0, 0, errors.names ? 12 : 24, 0)}
          ></InputMaterial>
          {errors.names && <ErrorMessage error={errors.names} />}
          <InputMaterial
            type="text"
            textLabel="Apellidos"
            {...kInputMaterial}
            focusBorderColor={
              errors.lastnames ? BorderColorEnum.ERROR : BorderColorEnum.PRIMARY
            }
            borderColor={
              errors.lastnames ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
            }
            register={register("lastnames", {
              required: {
                value: true,
                message: error.EMPLOYEE.ERRORS.lastnames.required,
              },
              min: {
                value: 3,
                message: error.EMPLOYEE.ERRORS.lastnames.min,
              },
            })}
            margin={marginREM(0, 0, errors.lastnames ? 12 : 24, 0)}
          ></InputMaterial>
          {errors.lastnames && <ErrorMessage error={errors.lastnames} />}
          <InputMaterial
            type="text"
            textLabel="Nro. de Documento"
            {...kInputMaterial}
            focusBorderColor={
              errors.documentNumber
                ? BorderColorEnum.ERROR
                : BorderColorEnum.PRIMARY
            }
            borderColor={
              errors.documentNumber
                ? BorderColorEnum.ERROR
                : BorderColorEnum.GRAY
            }
            register={register("documentNumber", {
              required: {
                value: true,
                message: error.EMPLOYEE.ERRORS.dni.required,
              },
              minLength: {
                value: 8,
                message: error.EMPLOYEE.ERRORS.dni.min,
              },
            })}
            margin={marginREM(0, 0, errors.documentNumber ? 12 : 24, 0)}
          ></InputMaterial>
          {errors.documentNumber && (
            <ErrorMessage error={errors.documentNumber} />
          )}
          <Heading {...kTitleCollapse} margin={marginREM(0, 0, 24, 0)}>
            Asignar usuario
          </Heading>
          <InputMaterial
            type="email"
            textLabel="Email"
            {...kInputMaterial}
            focusBorderColor={
              errors.email ? BorderColorEnum.ERROR : BorderColorEnum.PRIMARY
            }
            borderColor={
              errors.email ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
            }
            register={register("email", {
              required: {
                value: true,
                message: error.EMPLOYEE.ERRORS.email.required,
              },
              pattern: {
                value: Pattern.EMAIL,
                message: error.EMPLOYEE.ERRORS.email.pattern,
              },
            })}
            margin={marginREM(0, 0, errors.email ? 12 : 24, 0)}
          ></InputMaterial>
          {errors.email && <ErrorMessage error={errors.email} />}
          <InputMaterial
            type="password"
            textLabel="Contraseña"
            {...kInputMaterial}
            focusBorderColor={
              errors.password ? BorderColorEnum.ERROR : BorderColorEnum.PRIMARY
            }
            borderColor={
              errors.password ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
            }
            register={register("password", {
              required: {
                value: true,
                message: error.EMPLOYEE.ERRORS.password.required,
              },
              minLength: {
                value: 8,
                message: error.EMPLOYEE.ERRORS.password.min,
              },
            })}
            margin={marginREM(0, 0, errors.password ? 12 : 24, 0)}
          />
          {errors.password && <ErrorMessage error={errors.password} />}
        </Container>
        <DrawerFooter
          {...kDrawerFooter}
          style={{
            bottom: 0,
          }}
        >
          <Container
            width="100%"
            isFlex={true}
            isFlexRow={true}
            justifyContent="space-between"
          >
            <div></div>
            <Container gap="20px" isFlex={true} isFlexRow={true}>
              <Button {...kButtonCancel} type="button" onClick={props.onClose}>
                Cancelar
              </Button>
              <Button {...kButtonPrimary} type="submit">
                Guardar empleado
              </Button>
            </Container>
          </Container>
        </DrawerFooter>
      </form>
    </>
  );
};
