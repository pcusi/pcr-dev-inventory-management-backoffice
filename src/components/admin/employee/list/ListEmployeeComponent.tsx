import { TableComponent } from "../../../../shared/table/Table";
import { Card } from "../../../../shared";
import { BackgroundLightEnum } from "../../../../infrastructure/BackgroundEnum";
import { BoxShadowEnum } from "../../../../infrastructure/BoxShadowEnum";
import { FC } from "react";
import { IEmployee } from "src/infrastructure/interfaces/components/IEmployee";
import { kTable } from "src/constants/kTable";
import { ITableHeaders } from "src/infrastructure/interfaces/ITable";
import { useSearch } from "src/hooks/search/useSearch";
import { usePagination } from "src/hooks/pagination/usePagination";
import { OptionsResponse } from "types/options_schema";
import { useDropdownSelector } from "src/hooks/search/useDropdownSelector";

export const ListEmployeeComponent: FC<IEmployee> = (props) => {
  const headers: ITableHeaders[] = [
    {
      header: "DNI",
      value: "documentNumber",
    },
    {
      header: "Información",
      value: "names",
    },
    {
      header: "Activo",
      value: "status",
    },
    {
      header: "Correo",
      value: "email",
    },
    {
      header: "Fecha de ingreso",
      value: "createdAt",
    },
    {
      header: "Acciones",
      value: "id",
    },
  ];

  const searcher = useSearch();
  const pagination = usePagination(props.employees?.length!);
  const MOCK_OPTIONS: OptionsResponse = {
    options: ["5", "10", "15"],
  };
  const dropdown = useDropdownSelector();

  return (
    <>
      <Card
        background={BackgroundLightEnum.LIGHT}
        boxShadow={BoxShadowEnum.SHADOW_GRAY}
        borderRadius={12}
      >
        <TableComponent
          placeholder={"Buscar empleado por dni, nombres o correo"}
          columns={headers}
          data={props.employees}
          currentPage={pagination.currentPage}
          totalPages={pagination.totalPages}
          totalRows={props.employees?.length}
          rowsPerPage={pagination.dataPerPage}
          offset={pagination.offset}
          nextPage={() => pagination.nextPage()}
          prevPage={() => pagination.prevPage()}
          onChange={(e) => searcher.setSearch(e.target.value)}
          options={MOCK_OPTIONS.options}
          isOpen={dropdown.isOpen}
          openSelect={dropdown.openSelect}
          selected={pagination.dataPerPage.toString()}
          selectedChange={(e) => {
            pagination.setDataPerPage(parseInt(e));
            dropdown.openSelect();
          }}
          value={searcher.search}
          keyObject={headers[1].value}
          {...kTable}
        ></TableComponent>
      </Card>
    </>
  );
};
