import { paddingREM } from "../../../infrastructure/PaddingConvertEnum";
import { BackgroundLightEnum } from "../../../infrastructure/BackgroundEnum";
import { BoxShadowEnum } from "../../../infrastructure/BoxShadowEnum";
import { Button, Container, Heading, Text } from "../../../shared";
import { pixelTOREM } from "../../../infrastructure/interfaces/IHeading";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "../../../infrastructure/FontFamilyEnum";
import { TextColorEnum } from "../../../infrastructure/TextColorEnum";
import { marginREM } from "../../../infrastructure/MarginConvertEnum";
import { ListEmployeeComponent } from "./list/ListEmployeeComponent";
import { IoAdd } from "react-icons/io5";
import { CreateEmployeeComponent } from "./create/CreateEmployeeComponent";
import { Drawer } from "src/shared/admin/drawer/Drawer";
import { useDrawerEventAction } from "src/hooks/drawer/useDrawerEventAction";
import { useGetEmployees } from "src/hooks/employee/useGetEmployees";
import { EmployeeResponse } from "types/employee_schema";
import { kButtonPrimary } from "src/constants/Button";

export const EmployeeComponent = () => {
  const drawer_event = useDrawerEventAction();

  const employees = useGetEmployees();

  const addEmployee = (response: EmployeeResponse[]) => {
    employees.setEmployees([...response]);
    drawer_event.handleDrawerToggle();
  };

  return (
    <>
      {drawer_event.isOpen && (
        <Drawer
          onClick={drawer_event.handleDrawerToggle}
          isOpen={drawer_event.isOpen}
          sideWidth={drawer_event.sideWidth}
        >
          <CreateEmployeeComponent
            addEmployee={addEmployee}
            onClose={drawer_event.handleDrawerToggle}
          />
        </Drawer>
      )}
      <Container
        padding={paddingREM(48, 32, 48, 32)}
        margin={marginREM(0, 0, 40, 0)}
        background={BackgroundLightEnum.LIGHT_GRAY}
        boxShadow={BoxShadowEnum.SHADOW_GRAY}
        isFlex={true}
        isFlexRow={true}
        justifyContent="space-between"
        alignItems="center"
      >
        <Container isFlex={true}>
          <Heading
            fontSize={pixelTOREM(28)}
            fontFamily={fontStyle("Inter", FontFamilyEnum.MEDIUM)}
            fontWeight={FontWeightEnum.MEDIUM}
            fontColor={TextColorEnum.PRIMARY_60}
            margin={marginREM(0, 0, 4, 0)}
          >
            Empleados
          </Heading>
          <Text
            fontSize={pixelTOREM(14)}
            fontFamily={fontStyle("Inter", FontFamilyEnum.THIN)}
            fontWeight={FontWeightEnum.THIN}
            fontColor={TextColorEnum.PRIMARY_40}
          >
            Gestionar empleados es más fácil con Sysinv.
          </Text>
        </Container>
        <Button
          {...kButtonPrimary}
          onClick={drawer_event.handleDrawerToggle}
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: "12px",
          }}
        >
          <IoAdd></IoAdd> Agregar empleado
        </Button>
      </Container>
      <Container padding={paddingREM(0, 32, 0, 32)}>
        <ListEmployeeComponent
          employees={employees.employees}
        ></ListEmployeeComponent>
      </Container>
    </>
  );
};
