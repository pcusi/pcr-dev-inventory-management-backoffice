import styled from "styled-components";
import { Sidebar } from "../../shared/admin/sidebar/Sidebar";
import { BackgroundLightEnum } from "../../infrastructure/BackgroundEnum";
import { ResponsiveSizeEnum } from "../../infrastructure/ResponsiveSizeEnum";
import { Header } from "../../shared/admin/header/Header";
import { paddingREM } from "../../infrastructure/PaddingConvertEnum";
import { BoxShadowEnum } from "../../infrastructure/BoxShadowEnum";
import { Container, Heading, Separator, Text } from "../../shared";
import { adminRoutes } from "../../routes/admin-routes";
import { Navigation } from "../../shared/admin/navigation/Navigation";
import { Route, Routes } from "react-router-dom";
import { ICard } from "../../infrastructure/interfaces/ICard";
import { useGetUser } from "src/hooks/useGetUser";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "src/infrastructure/FontFamilyEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";
import { marginREM } from "src/infrastructure/MarginConvertEnum";
import { AvatarEnum } from "src/infrastructure/AvatarEnum";

const Admin = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: row;
  height: 100%;
`;

const Main = styled.main<ICard>`
  position: relative;
  background: ${(props) => (props.background ? props.background : "#fff")};
  margin-left: ${ResponsiveSizeEnum.ADMIN_WIDTH};
  width: calc(100% - ${ResponsiveSizeEnum.ADMIN_WIDTH});
  display: flex;
  overflow-y: auto;
  flex-direction: column;
  height: 100%;
`;

const MainWrapper = styled.div<ICard>`
  padding: ${(props) => (props.padding ? props.padding : "0")};
  width: ${(props) => (props.width ? props.width : "auto")};
  height: ${(props) => (props.height ? props.height : "auto")};
  background: ${(props) =>
    props.background ? props.background : "transparent"};
  min-width: ${(props) => (props.minWidth ? props.minWidth : "auto")};
  overflow-y: auto;
  background: ${(props) =>
    props.background ? props.background : "transparent"};
`;

export const AdminComponent = () => {
  const employee = useGetUser();

  return (
    <>
      <Admin>
        <Sidebar
          width={ResponsiveSizeEnum.ADMIN_WIDTH}
          maxWidth={ResponsiveSizeEnum.ADMIN_WIDTH}
          background={BackgroundLightEnum.PRIMARY_80}
          padding={paddingREM(32, 32, 32, 32)}
          style={{
            zIndex: 99,
          }}
        >
          <Separator
            width="100%"
            height="1px"
            background={BackgroundLightEnum.GRAY_20}
            margin={marginREM(0, 0, 24, 0)}
          ></Separator>
          <Container
            isFlex={true}
            isFlexRow={false}
            justifyContent="center"
            alignItems="center"
          >
            <Container
              width={AvatarEnum.WIDTH_XL}
              height={AvatarEnum.HEIGHT_XL}
              borderRadius={999}
              isFlex={true}
              isFlexRow={true}
              justifyContent="center"
              alignItems="center"
              background="white"
              margin={marginREM(0, 0, 12, 0)}
            ></Container>
            <Heading
              fontFamily={fontStyle("Inter", FontFamilyEnum.MEDIUM)}
              fontWeight={FontWeightEnum.MEDIUM}
              fontSize="1rem"
              fontColor={TextColorEnum.WHITE}
              margin={marginREM(0, 0, 4, 0)}
            >
              {employee?.names}
            </Heading>
            <Text
              fontFamily={fontStyle("Inter", FontFamilyEnum.MEDIUM)}
              fontWeight={FontWeightEnum.MEDIUM}
              fontSize=".875rem"
              fontColor={TextColorEnum.GRAY_20}
            >
              {employee?.role}
            </Text>
          </Container>
          <Separator
            width="100%"
            height="1px"
            background={BackgroundLightEnum.GRAY_20}
            margin={marginREM(24, 0, 24, 0)}
          ></Separator>
          <Navigation navigation={adminRoutes} />
        </Sidebar>
        <Main background={BackgroundLightEnum.LIGHT_GRAY}>
          <Header
            width="100%"
            height="auto"
            padding={paddingREM(24, 32, 24, 32)}
            background={BackgroundLightEnum.LIGHT_80}
            boxShadow={BoxShadowEnum.SHADOW_HEADER}
            hasBlur={true}
            position="fixed"
            style={{
              zIndex: 9,
              top: 0,
              right: 0,
            }}
          >
            <Container
              width="100%"
              isFlex={true}
              isFlexRow={true}
              justifyContent="space-between"
              alignItems="center"
            >
              <div style={{}} />
              <Container
                width="30px"
                height="30px"
                borderRadius={10}
                background="red"
                cursor="pointer"
              />
            </Container>
          </Header>
          <MainWrapper
            width="100%"
            minWidth="900px"
            padding={paddingREM(78, 0, 32, 0)}
          >
            <Routes>
              {adminRoutes.map(({ id, path, Component }) => (
                <Route key={id} path={path} element={<Component />} />
              ))}
            </Routes>
          </MainWrapper>
        </Main>
      </Admin>
    </>
  );
};
