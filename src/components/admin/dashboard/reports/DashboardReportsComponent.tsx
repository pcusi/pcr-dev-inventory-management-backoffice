import React from "react";
import { Card, Container, Heading } from "../../../../shared";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";
import { BoxShadowEnum } from "src/infrastructure/BoxShadowEnum";
import { IDashboardReports } from "src/infrastructure/interfaces/components/IDashboardReports";
import { AvatarEnum } from "src/infrastructure/AvatarEnum";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "src/infrastructure/FontFamilyEnum";
import { marginREM } from "src/infrastructure/MarginConvertEnum";
import { BackgroundLightEnum } from "src/infrastructure/BackgroundEnum";
import { TextColorEnum } from "src/infrastructure/TextColorEnum";
import {
  IoDocumentsOutline,
  IoAnalyticsOutline,
  IoBarChartOutline,
  IoPersonOutline,
} from "react-icons/io5";

export function generateBackround(key: string) {
  switch (key) {
    case "providers":
      return BackgroundLightEnum.GRADIENT_PROVIDERS;
    case "customers":
      return BackgroundLightEnum.GRADIENT_CUSTOMERS;
    case "kardex_entry":
      return BackgroundLightEnum.GRADIENT_ENTRY;
    case "kardex_exit":
      return BackgroundLightEnum.GRADIENT_EXIT;
  }
}

export function generateIcon(key: string) {
  switch (key) {
    case "providers":
      return <IoDocumentsOutline />;
    case "customers":
      return <IoPersonOutline />;
    case "kardex_entry":
      return <IoAnalyticsOutline />;
    case "kardex_exit":
      return <IoBarChartOutline />;
  }
}

export const DashboardReportsComponent: React.FC<IDashboardReports> = (
  props
) => {
  return (
    <Container
      gap="40px"
      style={{
        display: "grid",
        gridTemplateColumns: "repeat(4, 1fr)",
      }}
    >
      {Object.entries(props.reports).map(([key, value]) => (
        <Card
          borderRadius={12}
          background={generateBackround(key)}
          boxShadow={BoxShadowEnum.SHADOW_GRAY}
          height="185px"
          key={Math.random()}
        >
          <Card
            padding={paddingREM(24, 24, 24, 24)}
            borderRadius={12}
            height="96%"
            boxShadow={BoxShadowEnum.SHADOW_GRAY}
          >
            <Container
              isFlex={true}
              isFlexRow={true}
              alignItems="center"
              gap="12px"
              margin={marginREM(0, 0, 12, 0)}
            >
              <Container
                width={AvatarEnum.WIDTH_L}
                height={AvatarEnum.HEIGHT_L}
                borderRadius={999}
                isFlex={true}
                isFlexRow={true}
                justifyContent="center"
                alignItems="center"
                background={generateBackround(key)}
                style={{
                  color: "white",
                  fontSize: "1.25rem",
                }}
              >
                {generateIcon(key)}
              </Container>
              <Heading
                fontFamily={fontStyle("Inter", FontFamilyEnum.REGULAR)}
                fontWeight={FontWeightEnum.REGULAR}
              >
                {value.title}
              </Heading>
            </Container>
            <Container
              isFlex={true}
              isFlexRow={true}
              justifyContent="center"
              alignItems="center"
            >
              <Heading
                fontFamily={fontStyle("Inter", FontFamilyEnum.SEMI_BOLD)}
                fontWeight={FontWeightEnum.SEMI_BOLD}
                fontSize="2rem"
                fontColor={TextColorEnum.PRIMARY_60}
                textAlign="center"
              >
                {value.count}
              </Heading>
            </Container>
          </Card>
        </Card>
      ))}
    </Container>
  );
};
