import { Container, Heading, Text } from "../../../shared";
import { paddingREM } from "../../../infrastructure/PaddingConvertEnum";
import { BoxShadowEnum } from "../../../infrastructure/BoxShadowEnum";
import { BackgroundLightEnum } from "../../../infrastructure/BackgroundEnum";
import { pixelTOREM } from "../../../infrastructure/interfaces/IHeading";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "../../../infrastructure/FontFamilyEnum";
import { TextColorEnum } from "../../../infrastructure/TextColorEnum";
import { marginREM } from "../../../infrastructure/MarginConvertEnum";
import { DashboardReportsComponent } from "./reports/DashboardReportsComponent";
import { useGetDashboardReports } from "../../../hooks/useGetDashboardReports";
import { useGetUser } from "src/hooks/useGetUser";

export const DashboardComponent = () => {
  const reports = useGetDashboardReports();
  const employee = useGetUser();

  return (
    <>
      <Container
        width="100%"
        padding={paddingREM(48, 32, 48, 32)}
        background={BackgroundLightEnum.LIGHT_GRAY}
        boxShadow={BoxShadowEnum.SHADOW_GRAY}
      >
        <Heading
          fontSize={pixelTOREM(28)}
          fontFamily={fontStyle("Inter", FontFamilyEnum.MEDIUM)}
          fontWeight={FontWeightEnum.MEDIUM}
          fontColor={TextColorEnum.PRIMARY_60}
          margin={marginREM(0, 0, 4, 0)}
        >
          Dashboard
        </Heading>
        <Text
          fontSize={pixelTOREM(14)}
          fontFamily={fontStyle("Inter", FontFamilyEnum.THIN)}
          fontWeight={FontWeightEnum.THIN}
          fontColor={TextColorEnum.PRIMARY_40}
        >
          Un gusto verte {employee?.names}, este es el detalle de la semana.
        </Text>
      </Container>

      <Container width="100%" padding={paddingREM(32, 32, 32, 32)}>
        <DashboardReportsComponent reports={reports} />
      </Container>
    </>
  );
};
