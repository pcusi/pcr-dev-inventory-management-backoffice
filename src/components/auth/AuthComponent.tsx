import {authRoutes} from "../../routes/auth-routes";
import {Route, Routes} from "react-router-dom";

export const AuthComponent = () => {
    return (
        <>
            <Routes>
                <Route>
                    {
                        authRoutes.map(({id, path, Component}) => (
                          <Route key={id} path={path} element={<Component />}></Route>
                        ))
                    }
                </Route>
            </Routes>
        </>
    )
}