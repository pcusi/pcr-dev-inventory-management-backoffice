import axios from "axios";
import React, { useContext } from "react";
import { Container } from "../../../shared/container/Container";
import { BackgroundLightEnum } from "../../../infrastructure/BackgroundEnum";
import {
  FontFamilyEnum,
  fontStyle,
  FontWeightEnum,
} from "../../../infrastructure/FontFamilyEnum";
import {
  Anchor,
  Heading,
  Text,
  Card,
  InputMaterial,
  CheckboxMaterial,
  Button,
} from "../../../shared/index";
import { BorderColorEnum } from "../../../infrastructure/BorderColorEnum";
import { TextColorEnum } from "../../../infrastructure/TextColorEnum";
import { useForm } from "react-hook-form";
import { environment } from "../../../environment/environment";
import { Spinner } from "../../../shared/spinner/Spinner";
import { marginREM } from "../../../infrastructure/MarginConvertEnum";
import { ErrorMessage } from "../../../shared/errors/Errors";
import * as iErrors from "../../../utils/json/errors.json";
import { useNavigate } from "react-router-dom";
import { EmployeeResponse } from "types/employee_schema";
import { AuthContext } from "src/context/AuthContext";
import { paddingREM } from "src/infrastructure/PaddingConvertEnum";

export const LoginComponent = () => {
  const [isFetching, setFetching] = React.useState(false);

  const error = JSON.parse(JSON.stringify(iErrors));

  const history = useNavigate();

  const authContext = useContext(AuthContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<EmployeeResponse>({
    mode: "onChange",
  });

  const onSubmit = handleSubmit(async (employee: EmployeeResponse) => {
    setFetching(true);

    axios({
      method: "POST",
      url: `${environment.dev_uri}/account/login`,
      data: {
        email: employee.email,
        password: employee.password,
      },
    })
      .then(({ data }) => {
        localStorage.setItem("authToken", data.token);
        authContext?.setUser({
          isAuthenticated: true,
          token: data.token,
        });
        setTimeout(() => {
          setFetching(false);
          history("/admin/dashboard", { replace: true });
        }, 1000);
      })
      .catch(({ response }) => console.log(response.data.message));
  });

  return (
    <>
      <Container isFlex={true} isFlexRow={true} height="100%" width="100%">
        <Container
          background={BackgroundLightEnum.LIGHT_GRAY}
          width="100%"
          isFlex={true}
          isFlexRow={true}
          alignItems="center"
          justifyContent="center"
          height="100%"
          padding="4rem"
        >
          <form onSubmit={onSubmit}>
            <Card
              background={BackgroundLightEnum.LIGHT}
              width="480px"
              isFlex={true}
              padding="2rem"
              borderRadius={8}
            >
              <Heading
                fontSize="1.75rem"
                fontFamily={fontStyle("Inter", FontFamilyEnum.MEDIUM)}
                fontWeight={FontWeightEnum.MEDIUM}
                fontColor={TextColorEnum.PRIMARY_60}
                textAlign="center"
                margin={marginREM(0, 0, 4, 0)}
              >
                Iniciar Sesión
              </Heading>
              <Text
                fontSize="0.875rem"
                fontFamily={fontStyle("Inter", FontFamilyEnum.THIN)}
                fontWeight={FontWeightEnum.THIN}
                fontColor={TextColorEnum.PRIMARY_40}
                textAlign="center"
                margin={marginREM(0, 0, 48, 0)}
              >
                Rellena los campos, para iniciar sesión correctamente.
              </Text>
              <InputMaterial
                background={BackgroundLightEnum.LIGHT}
                fontSize="0.875rem"
                fontFamily={fontStyle("Inter", FontFamilyEnum.LIGHT)}
                fontWeight={FontWeightEnum.LIGHT}
                fontColor={TextColorEnum.PRIMARY_40}
                focusBorderColor={
                  errors.email ? BorderColorEnum.ERROR : BorderColorEnum.PRIMARY
                }
                borderColor={
                  errors.email ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
                }
                margin={marginREM(0, 0, errors.email ? 12 : 24, 0)}
                type="email"
                textLabel="Email"
                register={register("email", {
                  required: {
                    value: true,
                    message: error.LOGIN.ERRORS.username.required,
                  },
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                    message: error.LOGIN.ERRORS.username.pattern,
                  },
                })}
              />
              {errors.email && <ErrorMessage error={errors.email} />}
              <InputMaterial
                background={BackgroundLightEnum.LIGHT}
                fontSize="0.875rem"
                fontFamily={fontStyle("Inter", FontFamilyEnum.LIGHT)}
                fontWeight={FontWeightEnum.LIGHT}
                fontColor={TextColorEnum.PRIMARY_40}
                focusBorderColor={
                  errors.password
                    ? BorderColorEnum.ERROR
                    : BorderColorEnum.PRIMARY
                }
                borderColor={
                  errors.password ? BorderColorEnum.ERROR : BorderColorEnum.GRAY
                }
                margin={marginREM(0, 0, errors.password ? 12 : 24, 0)}
                type="password"
                textLabel="Contraseña"
                register={register("password", {
                  required: {
                    value: true,
                    message: error.LOGIN.ERRORS.password.required,
                  },
                })}
              />
              {errors.password && <ErrorMessage error={errors.password} />}
              <Container
                width="100%"
                isFlex={true}
                isFlexRow={true}
                alignItems="center"
                justifyContent="space-between"
                margin={marginREM(0, 0, 48, 0)}
              >
                <CheckboxMaterial
                  width="43%"
                  fontFamily={fontStyle("Inter", FontFamilyEnum.THIN)}
                  fontWeight={FontWeightEnum.THIN}
                  fontSize="0.875rem"
                  value="Recordarme por 14 días"
                />
                <Anchor
                  fontFamily={fontStyle("Inter", FontFamilyEnum.REGULAR)}
                  fontWeight={FontWeightEnum.REGULAR}
                  fontSize="0.875rem"
                  fontColor={TextColorEnum.PRIMARY_20}
                >
                  Olvidaste la contraseña?
                </Anchor>
              </Container>
              <Button
                type="submit"
                width="100%"
                background={BackgroundLightEnum.PRIMARY_20}
                backgroundHover={BackgroundLightEnum.PRIMARY}
                fontSize="1rem"
                fontFamily={fontStyle("Inter", FontFamilyEnum.REGULAR)}
                fontWeight={FontWeightEnum.REGULAR}
                fontColor={TextColorEnum.WHITE}
                padding={paddingREM(16, 16, 16, 16)}
                borderRadius={8}
              >
                <Container
                  isFlex={true}
                  isFlexRow={true}
                  alignItems="center"
                  background="none"
                  justifyContent="center"
                  gap="12px"
                >
                  <span>Iniciar Sesión</span>
                  {isFetching && (
                    <Spinner
                      width="20px"
                      height="20px"
                      borderLeftColor={BackgroundLightEnum.LIGHT}
                    />
                  )}
                </Container>
              </Button>
            </Card>
          </form>
        </Container>
      </Container>
    </>
  );
};
