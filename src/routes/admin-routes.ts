import { NavigationRoute } from "../infrastructure/routes/NavigationRoute";
import { MdSpaceDashboard } from "react-icons/md";
import { DashboardComponent } from "../components/admin/dashboard/DashboardComponent";
import { EmployeeComponent } from "../components/admin/employee/EmployeeComponent";

export const adminRoutes: NavigationRoute[] = [
  {
    id: 0,
    name: "Dashboard",
    to: "/admin/dashboard",
    path: "dashboard",
    Icon: MdSpaceDashboard,
    Component: DashboardComponent,
  },
  {
    id: 1,
    name: "Empleados",
    to: "/admin/employee",
    path: "employee",
    Icon: MdSpaceDashboard,
    Component: EmployeeComponent,
  },
];
