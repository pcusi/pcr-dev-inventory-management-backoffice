import {NavigationRoute} from "../infrastructure/routes/NavigationRoute";
import {LoginComponent} from "../components/auth/login/LoginComponent";

import {
    MdLocationCity
} from 'react-icons/md';

export const authRoutes: NavigationRoute[] = [
    {
        id: 0,
        name: "Login",
        to: "auth/login",
        Icon: MdLocationCity,
        path: "login",
        Component: LoginComponent
    }
]