import React from "react";
import { BackofficeLayout } from "./components/BackofficeLayout";
import { AuthContextProvider } from "./context/AuthContext";

export const App = () => {
  return (
    <>
      <AuthContextProvider>
        <div className="layout">
          <BackofficeLayout></BackofficeLayout>
        </div>
      </AuthContextProvider>
    </>
  );
};
