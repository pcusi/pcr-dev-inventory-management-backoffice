import React, { createContext, useState } from "react";

export type AuthType = {
    isAuthenticated: boolean;
    token: string | null;
}

export type AuthContextType = {
    user: AuthType | null;
    setUser: React.Dispatch<React.SetStateAction<AuthType | null>>;
}

export type AuthenticationProvider = {
    children: React.ReactNode;
}

export const AuthContext = createContext<AuthContextType | null>(null);

export const AuthContextProvider = ({ children }: AuthenticationProvider) => {

    const [user, setUser] = useState<AuthType | null>({
        isAuthenticated: false,
        token: null
    });

    return (
        <AuthContext.Provider value={{ user, setUser }}>
            {children}
        </AuthContext.Provider> 
    )
}